# Renaming Episodes

A script allowing users to rename episodes from a TVShow with a Plex friendly nomenclature.

Usage :

>  -t      (optional) Sets the name of your TVShow in your filename.

>   -s      Sets the season number of the episodes you want to rename.

>   -e      (optional) Sets the starting offset of your episode list. Default is 1.

Exemple : sh rename-episode.sh -t "One Piece" -s 1 -e 50
