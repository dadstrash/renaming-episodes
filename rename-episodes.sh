#!/bin/bash

#Script that renames episodes from your current directory, in a Plex-friendly format.

#command sample : sh rename-episode.sh -t 'One Piece' -s 1 -e 50

###################################### functions ##############################################
help () {
    #help function. Tells you what to do.
    echo 'Usage : \n'
    echo '  -t      (optional) Sets the name of your TVShow in your filename.\n'
    echo '  -s      Sets the season number of the episodes you want to rename. \n'
    echo '  -e      (optional) Sets the starting offset of your episode list. Default is 1. \n'
    echo 'Exemple : sh rename-episode.sh -t "One Piece" -s 1 -e 50'
}


###################################### controls&settings ###############################################
no_args="true"
#checking all parameters for control
while getopts ":t:s:e:h" option; do
    case $option in
        t) # setting tvshow
            show=$OPTARG' - ';;
        s) # setting season
            season_number=$OPTARG;;
        e) # setting episode offset
            episode_number=$OPTARG;;
        h) # calls help function
            help
            exit 0;;
        \?) #invalid arg
            echo "Invalid Option: -$OPTARG" 1>&2
            help
            exit 1
            ;;
   esac
   #there was at least one arg
   no_args="false"
done

#if no args, then exit
[[ "$no_args" == "true" ]] && { echo 'Invalid use of the renaming script : no args. \n'; help; exit 1; }

#if season number is not set, exit
if [ -z "$season_number" ]; then
    echo 'No season number defined, programm will stop. \n'
    echo 'As a reminder : \n'
    help

    exit 1
fi

###################################### main programm ###########################################

#season format
season='S'$season_number
echo 'Season defined : '$season'\n'

#episode format
episode_prefix='E'

#default value for episode offset.
num=1

#we test if the episode offset has been set.
if [ -n "$episode_number" ]; then
    echo "An episode offset has been defined :"$episode_number", let's use it.\n"
    num=$episode_number
else
    echo "No episode offset, let's start at the begining then.\n"
fi

#start of the renamming process
for episode in *; do
echo $episode

    #we detect the file extension
    extension=${episode##*.}
    #build the final filename
    rename="$show$season$episode_prefix$num.$extension"
    echo "Rename '$episode' to '$rename'\n"
    #renaming
    mv "$episode" "$rename"
    
    ((num++))
done

exit 0 

